function vm(email, money, drinkChoice = "coffee") {
    let kembalian;

    switch (drinkChoice) {
        case "mineral water":
            kembalian = money - 5000;
            break;
        case "cola":
            kembalian = money - 7500;
            break;
        case "coffee":
            kembalian = money - 12250;
            break;
        default:
            kembalian = "Error";
            break;
    }
    
    if (!email && money != null) {
        console.log("Sorry can't process it until your email filled");
    } else if (typeof email != "string" && money != null) {
        console.log("Invalid input email");
    } else if (email != null && !money) {
        console.log("Sorry can't process it until you put your money");
    } else if (email != null && typeof money != "number") {
        console.log("Invalid input money");
    } else if (email != null && money != null && kembalian >= 0) {
        console.log(`Welcome ${email} to May's Vending Machine. Your choice is ${drinkChoice} , here's your drink Your change are ${kembalian} Thank you`);
    } else if (email != null && money != null && kembalian < 0) {
        console.log(`Welcome ${email} to May's Vending Machine Sorry, insufficient balance we can't process your ${drinkChoice} You need ${kembalian *= -1} more to buy this drink Thank you `);
    } else if (email != null && money != null && kembalian == "Error") {
        console.log(`Welcome ${email} to May's Vending Machine Sorry, we can't process your ${drinkChoice} cause not found in the menu, Thank you `);
    } else {
        console.log("Please check your input");
    }

}

vm("g2academy@mail.com", 2000, "test");